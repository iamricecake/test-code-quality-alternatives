z = 1

def foo(a,b,c,d,e,f,g)
  if z
    puts z
    if z
      puts z
    end
  end
end

def bar
  if z
    puts z
    if z
      puts z
    end
  end

  if z || z == 3 || z == 5
    if z > 3
      puts 'z greater than 3'
    end
  else
    if z == 123
      puts 123
    end
  end
end

def baz(x)
  case x
  when 1
    return 1
  when 2
    return 2
  when 3
    return 3
  when 4
    return 4
  when 5
    return 5
  when 6
    return 6
  else
    return 99
  end
end

foo(1,2,3,4,5,6,7)
bar
