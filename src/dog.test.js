const assert = require('assert');
const { Dog } = require('./dog');

describe('Dog', function() {
  describe('#bark 1', function() {
    it('barks', function() {
      dog = new Dog();
      assert.equal(dog.bark(), 'Bark')
    });
  });

  describe('#jump 2', function() {
    it('jumps', function() {
      dog = new Dog();
      assert.equal(dog.jump(), 'Jumpssss')
    });
  });

  describe('#eat 3', function() {
    it('eats', function() {
      dog = new Dog();
      assert.equal(dog.eat(), 'Eatxxx')
    });
  });
});
